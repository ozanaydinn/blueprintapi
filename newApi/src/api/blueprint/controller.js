import { success, notFound } from '../../services/response'
import { Blueprint } from '.'
import User from '../user/model'

export const show = ({ params }, res, next) =>
  Blueprint.findById(params.id)
    .then(notFound(res))
    .then((blueprint) => blueprint ? blueprint.view() : null)
    .then(success(res))
    .catch(next)

export const create = ({ bodymen: { body } }, res, next) =>
  Blueprint.create(body)
    .then((blueprint) => blueprint.view(true))
    .then(blueprint => addBlueprintToUser(blueprint))
    .then(success(res, 201))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Blueprint.findById(params.id)
    .then(notFound(res))
    .then((blueprint) => blueprint ? Object.assign(blueprint, body).save() : null)
    .then((blueprint) => blueprint ? blueprint.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Blueprint.findById(params.id)
    .then(notFound(res))
    .then((blueprint) => blueprint ? blueprint.remove() : null)
    .then(success(res, 204))
    .catch(next)

const addBlueprintToUser = (blueprint) => {
  User.findByIdAndUpdate(blueprint.owner, { $push: { "blueprints": blueprint.id }, new: true })
  return blueprint;
}
