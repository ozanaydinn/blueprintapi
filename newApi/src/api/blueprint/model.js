import mongoose, { Schema } from 'mongoose'

const blueprintSchema = new Schema({
  name: {
    type: String,
    default: "Blueprint",
    required: true,
    maxlength: 120
  },
  blueprint: {
    type: String,
    required: true
  },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
}, {
    timestamps: true
  })

blueprintSchema.methods = {
  view() {
    let view = {}
    let fields = ['id', 'name', 'blueprint', 'owner']

    fields.forEach((field) => { view[field] = this[field] })

    return view
  }
}

const model = mongoose.model('Blueprint', blueprintSchema)

export const schema = model.schema
export default model
