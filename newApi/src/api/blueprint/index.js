import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { password as token } from '../../services/passport'
import { show, create, update, destroy } from './controller'
import { schema } from './model'
export Blueprint, { schema } from './model'

const router = new Router()
const { name, blueprint, owner } = schema.tree

router.get('/:id',
  token({ required: true }),
  show)

router.post('/',
  body({ blueprint, owner, name }),
  create)

router.put('/:id',
  token({ required: true }),
  body({ name, blueprint }),
  update)

router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
